
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Machine Learning Slides

- Omar Trejo
- August, 2016

At some point I was asked to give a presentation on Random Forests and Boosting
for a client and I decided to create a repository were any subsequent Machine
Learning slides will be. The repository is open so that anyone can take
something if it's useful for them, and can contribute back if they wish.

---

> "The best ideas are common property."
>
> —Seneca
